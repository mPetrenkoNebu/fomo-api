const express = require('express');
const bodyParser = require('body-parser');
const csrf = require('csurf');
const cors = require('cors');
const cookieParser = require('cookie-parser');

//const config = require('./config')

const csrfMiddleware = csrf({cookie: true})


const whitelistController = require('./controllers/whitelistController')
const airdropController = require('./controllers/airdropController')
const newsController = require('./controllers/newsController')
const userController = require('./controllers/userController')
const tagsController = require('./controllers/tagsController')
const instructionController = require('./controllers/instructionController')
const typeController = require('./controllers/typesController')

const PORT = process.env.PORT || 8080;
const app = express();

app.use(cors())
app.use(bodyParser.json({limit: '50mb'}));

app.use(cookieParser());
// app.use(csrfMiddleware);


 // app.all('*', (req, res, next) => {
//     res.cookie("XSRF-TOKEN", req.csrfToken());
//     next();
// });



app.use('/whitelist', whitelistController.route);
app.use('/airdrop', airdropController.route);
app.use('/news', newsController.route);
app.use('/user', userController.route);
app.use('/tags', tagsController.route);
app.use('/instruction', instructionController.route);
app.use('/type', typeController.route);

app.listen(PORT, () => {
    console.log(`App is listening at port ${PORT}`);
});

