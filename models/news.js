class News {
    constructor(id, title,content, photo, text,  tags, comments,data, creatorId, creatorName) {
        this.id=id;
        this.title=title;
        this.content=content;
        this.photo = photo;
        this.text = text;
        this.tags = tags;
        this.comments = comments;
        this.data = data;
        this.creatorId = creatorId;
        this.creatorName = creatorName;
    }
}

module.exports = News;