class Airdrop {
    constructor(id, isFilled, iconUrl, project, reward,website, types,searchTypes,  ends,starts, description,status, creatorId, creatorName) {
        this.id = id;
        this.isFilled = isFilled;
        this.iconUrl = iconUrl;
        this.project = project;
        this.reward = reward;
        this.website = website;
        this.types = types;
        this.searchTypes = searchTypes;
        this.ends = ends;
        this.starts = starts;
        this.description = description;
        this.status = status;
        this.creatorId = creatorId;
        this.creatorName = creatorName;
    }
}

module.exports = Airdrop;