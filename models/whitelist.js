class Whitelist {
    constructor(id, isFilled, iconUrl, name, website, platform, hardCap, ends, description,instructions,  creatorId, creatorName) {
        this.id = id;
        this.isFilled = isFilled;
        this.iconUrl = iconUrl;
        this.name = name;
        this.website = website;
        this.platform = platform;
        this.hardCap = hardCap;
        this.ends = ends;
        this.description = description;
        this.instructions = instructions;
        this.creatorId = creatorId;
        this.creatorName = creatorName;
    }
}

module.exports = Whitelist;