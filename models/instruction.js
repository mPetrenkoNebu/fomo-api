class Instruction {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
}

module.exports = Instruction;