class Type {
    constructor(id, color, name) {
        this.id = id;
        this.color = color;
        this.name = name;
    }
}

module.exports = Type;