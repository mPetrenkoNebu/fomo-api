class User {
    constructor(id, email, isAdmin, filledWhitelists, filledAirdrops) {
        this.id = id;
        this.email = email;
        this.isAdmin = isAdmin;
        this.filledWhitelists = filledWhitelists;
        this.filledAirdrops = filledAirdrops;
    }
}

module.exports = User;