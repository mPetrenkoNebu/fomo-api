const firebase = require('firebase')
// const config = require('./config')

const db = firebase.initializeApp({
    apiKey: "AIzaSyC6lvAFYQMRcVNUHTuzGSc0Vcv0cBAMr60",
    authDomain: "fomo-61b78.firebaseapp.com",
    databaseURL: "https://fomo-61b78-default-rtdb.firebaseio.com",
    projectId: "fomo-61b78",
    storageBucket: "fomo-61b78.appspot.com",
    messagingSenderId: "722089357509",
    appId: "1:722089357509:web:cf04c718a4cb98ac8000ac",
    measurementId: "G-G5DZTGV5S3"
})

module.exports = db;