const express = require('express');
const router = express.Router();
const firebase = require('../db');
const Type = require('../models/type')
const firestore = firebase.firestore();

router.get('/', async (req, res) => {
    try{
        const types = await firestore.collection('types');
        const data = await types.get();
        const typesArray = [];
        if(data.empty){
            res.status(404).send("No whitelists");
        }else{
            data.forEach(doc => {
                const type = new Type(
                    doc.id,
                    doc.data().color,
                    doc.data().name
                );
                typesArray.push(type)
            })
            res.send(typesArray)
        }
    }catch (err) {
        res.status(400).send(err.message)
    }
})

module.exports.route = router;