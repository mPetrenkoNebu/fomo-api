const express = require('express');
const router = express.Router();
const adminFirebase = require('../adminFirebase');
const firebase = require('../db');
const firebaseRef = require('firebase')
const firestore = firebase.firestore();

router.post('/login', async (req, res) => {
    const idToken = req.headers.authorization ;
    const userName = req.body.userName;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token!");

    const token = words[1];

    adminFirebase.auth().verifyIdToken(token).then((decodedToken) => {
        return decodedToken;
    }).then(async (result) => {
        const data = await firestore.collection('users').doc(result.uid);
        const user = await data.get();
        if(user.exists){
            res.send(user.data())
        }
        else{
            return result
        }
    }).then((result) => {
        if(result){
            const user = {uid: result.uid, name: userName, email: result.email, isAdmin: false, filledWhitelists: [], filledAirdrops: [], instructions: [{name: "Profile 1",  items: [{name: "ETH", value: ""}, {name: "Twitter", value: ""}, {name: "Telegram", value: ""}]}]}
            firestore.collection('users').doc(result.uid).set(user).then(() => {
                res.send(user)
            })
        }
    }).catch((err) => {
        console.log(err)
    })
})
router.get('/check-user/:userName', async (req, res) => {
    const userName = req.params.userName;
    const users = await firestore.collection('users');
    const isUser = await users.where("name", "==", userName).get();
    if(!isUser.empty) {
        res.status(409).send({message: "User with this user name is already exists!"})
    }else{
        res.status(200).send("Success!")
    }
})
router.put('/filled', async (req, res) => {
    const idToken = req.headers.authorization ;
    const filledId = req.body.filledId;
    const isFilled = req.body.isFilled;
    const isWhitelist = req.body.isWhitelist;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send({message: "Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!"});

    const token = words[1];

    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const data = await firestore.collection('users').doc(user.uid);
        if(isFilled){
            if(isWhitelist){
                await data.update({
                    filledWhitelists: firebaseRef.firestore.FieldValue.arrayUnion(filledId)
                });
            }else{
                await data.update({
                    filledAirdrops: firebaseRef.firestore.FieldValue.arrayUnion(filledId)
                });
            }
        }else{
            if(isWhitelist){
                await data.update({
                    filledWhitelists: firebaseRef.firestore.FieldValue.arrayRemove(filledId)
                });
            }else{
                await data.update({
                    filledAirdrops: firebaseRef.firestore.FieldValue.arrayRemove(filledId)
                });
            }

        }
        const userData = await data.get();
        res.send(userData.data())
    }catch (err) {
        res.status(400).send(err.message)
    }
})
router.put('/instruction', async (req, res) => {
    const idToken = req.headers.authorization ;
    const instructions = req.body.instructions;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];

    try {
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const data = await firestore.collection('users').doc(user.uid);
        await data.update({
            instructions: instructions
        })
        const userData = await data.get();
        res.send(userData.data())
    }catch (err) {
        res.status(400).send(err.message)
    }
})


module.exports.route = router;