const express = require('express');
const router = express.Router();
const firebase = require('../db');
const firebaseRef = require('firebase')
const News = require('../models/news');
const adminFirebase = require('../adminFirebase');
const firestore = firebase.firestore();

router.get('/all', async (req, res) => {
    try{
        const newsCollection = await firestore.collection('news').orderBy('data', 'desc');
        const data = await newsCollection.get();
        const newsListsArray = [];
        if(data.empty){
            res.status(404).send("No whitelists");
        }else{
            data.forEach(doc => {
                const news = new News(
                    doc.id,
                    doc.data().title,
                    doc.data().content,
                    doc.data().photo,
                    doc.data().text,
                    doc.data().tags ,
                    doc.data().comments ,
                    doc.data().data ,
                    doc.data().creatorId ,
                    doc.data().creatorName ,
                );
                newsListsArray.push(news)
            })
            res.send(newsListsArray)
        }
    }catch (err) {
        res.status(400).send(err.message)
    }
})
router.post('/add-news', async (req, res) => {
    const idToken = req.headers.authorization ;
    const data = req.body;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];

    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const currentUserData = await firestore.collection('users').doc(user.uid);
        const currentUser = await currentUserData.get()
        if(!currentUser.data().isAdmin)
            res.status(401).send("UNAUTHORIZED REQUEST!");
        else{
            firestore.collection('news').doc().set(data).then((response) => {
                res.send(data)
            })
        }
    }catch (err) {
        res.status(400).send(err.message)
    }
});
router.post('/add-comment', async (req, res) => {
    const idToken = req.headers.authorization ;
    const comment = req.body.comment;
    const newsId = req.body.newsId;


    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];

    try{
        const user = await adminFirebase.auth().verifyIdToken(token);
        const data = await firestore.collection('news').doc(newsId);
        await data.update({
            comments: firebaseRef.firestore.FieldValue.arrayUnion(comment)
        });
        res.status(200).send()
    }catch (err) {
        res.status(400).send(err.message)
    }
})
router.get('/:id', async (req,res) => {
    const idToken = req.headers.authorization ;
    const id = req.params.id;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];

    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const news = await firestore.collection("news").doc(id)
        const data = await news.get();
        if(!data.exists){
            res.status(404).send(`Airdrop not found`)
        }else{
            res.send(data.data())
        }
    }catch (err){
        res.status(400).send(err.message)
    }
})
router.put('/:id', async (req,res) => {
    const idToken = req.headers.authorization ;
    const id = req.params.id;
    const data = req.body;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];
    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const currentUserData = await firestore.collection('users').doc(user.uid);
        const currentUser = await currentUserData.get()
        if(!currentUser.data().isAdmin)
            res.status(401).send("UNAUTHORIZED REQUEST!");
        else{
            const news = await firestore.collection("news").doc(id);
            await news.update(data);
            res.send("News record updated successfuly")
        }
    }catch (err){
        res.status(400).send(err.message)
    }
})
router.delete('/:id', async (req, res) => {
    const idToken = req.headers.authorization ;
    const id = req.params.id;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];
    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const currentUserData = await firestore.collection('users').doc(user.uid);
        const currentUser = await currentUserData.get()
        if(!currentUser.data().isAdmin)
            res.status(401).send("UNAUTHORIZED REQUEST!");
        else{
            await firestore.collection("news").doc(id).delete();
            res.send("News record delete successfuly")
        }
    }catch (err){
        res.status(400).send(err.message)
    }
})

module.exports.route = router;