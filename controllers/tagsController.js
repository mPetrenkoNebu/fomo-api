const express = require('express');
const router = express.Router();
const firebase = require('../db');
const Tag = require('../models/tag')
const firestore = firebase.firestore();

router.get('/', async (req, res) => {
    try{
        const tags = await firestore.collection('tags');
        const data = await tags.get();
        const tagsArray = [];
        if(data.empty){
            res.status(404).send("No whitelists");
        }else{
            data.forEach(doc => {
                const tag = new Tag(
                    doc.id,
                    doc.data().tag
                );
                tagsArray.push(tag)
            })
            res.send(tagsArray)
        }
    }catch (err) {
        res.status(400).send(err.message)
    }
})

module.exports.route = router;