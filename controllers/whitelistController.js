const express = require('express');
const router = express.Router();
const firebase = require('../db');
const Whitelist = require('../models/whitelist');
const firestore = firebase.firestore();
const adminFirebase = require('../adminFirebase');


router.get('/all', async (req, res) => {
    try{
       const whitelists = await firestore.collection('whitelists');
       const data = await whitelists.get();
       const whiteListsArray = [];
       if(data.empty){
           res.status(404).send("No whitelists");
       }else{
           data.forEach(doc => {
               const whitelist = new Whitelist(
                   doc.id,
                   doc.data().isFilled,
                   doc.data().iconUrl ,
                   doc.data().name ,
                   doc.data().website ,
                   doc.data().platform ,
                   doc.data().hardCap ,
                   doc.data().ends ,
                   doc.data().description ,
                   doc.data().instructions ,
                   doc.data().creatorId ,
                   doc.data().creatorName ,
               );
               whiteListsArray.push(whitelist)
           })
           res.send(whiteListsArray.sort((a, b) => (a.ends > b.ends) ? 1 : -1))
       }
    }catch (err) {
        res.status(400).send(err.message)
    }
})
router.post('/add-whitelist', async (req, res) => {
    const idToken = req.headers.authorization ;
    const data = req.body;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];
    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const currentUserData = await firestore.collection('users').doc(user.uid);
        const currentUser = await currentUserData.get()
        if(!currentUser.data().isAdmin)
            res.status(401).send("UNAUTHORIZED REQUEST!");
        else{
            const whitelist = await  firestore.collection('whitelists').doc()
            whitelist.set(data).then((response) => {
                res.send(response)
            })
        }
    }catch (err) {
        res.status(400).send(err.message)
    }
})
router.get("/:id", async  (req, res)=> {
    const idToken = req.headers.authorization ;
    const id = req.params.id;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];
    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const whitelist = await firestore.collection("whitelists").doc(id)
        const data = await whitelist.get();
        if(!data.exists){
            res.status(404).send(`Whitelist not found`)
        }else{
            res.send(data.data())
        }
    }catch (err){
        res.status(400).send(err.message)
    }
});
router.get("/calendar/:startDay&:endaDay", async  (req, res)=> {
    try{
        const startDay = req.params.startDay;
        const endaDay = req.params.endaDay;
        const whitelist = await firestore.collection("whitelists");
        const filteredWhitelist =await whitelist.orderBy("ends").where("ends", ">=" ,parseInt(startDay)).where("ends", "<=" ,parseInt(endaDay))
        const data = await filteredWhitelist.get();
        const whiteListsArray = [];
        if(data.empty){
            res.status(404).send("No whitelists");
        }else{
            data.forEach(doc => {
                const whitelist = new Whitelist(
                    doc.id,
                    doc.data().isFilled,
                    doc.data().iconUrl ,
                    doc.data().name ,
                    doc.data().website ,
                    doc.data().platform ,
                    doc.data().hardCap ,
                    doc.data().ends ,
                    doc.data().description ,
                    doc.data().instructions ,
                    doc.data().creatorId ,
                    doc.data().creatorName ,
                );
                whiteListsArray.push(whitelist)
            })
            res.send(whiteListsArray)
        }
    }catch (err){
        res.status(400).send(err.message)
    }
})
router.put('/:id', async (req, res) => {
    const idToken = req.headers.authorization ;
    const id = req.params.id;
    const data = req.body;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];
    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const currentUserData = await firestore.collection('users').doc(user.uid);
        const currentUser = await currentUserData.get()
        if(!currentUser.data().isAdmin)
            res.status(401).send("UNAUTHORIZED REQUEST!");
        else{
            const whitelist = await firestore.collection("whitelists").doc(id);
            await whitelist.update(data);
            res.send("Whitelist record updated successfuly")
        }
    }catch (err){
        res.status(400).send(err.message)
    }
})
router.delete('/:id', async (req, res) => {
    const idToken = req.headers.authorization ;
    const id = req.params.id;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];
    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const currentUserData = await firestore.collection('users').doc(user.uid);
        const currentUser = await currentUserData.get()
        if(!currentUser.data().isAdmin)
            res.status(401).send("UNAUTHORIZED REQUEST!");
        else{
            await firestore.collection("whitelists").doc(id).delete();
            res.send("Whitelist record delete successfuly")
        }
    }catch (err){
        res.status(400).send(err.message)
    }
})
module.exports.route = router;
