const express = require('express');
const router = express.Router();
const firebase = require('../db');
const Airdrop = require('../models/airdrop');
const firestore = firebase.firestore();
const adminFirebase = require('../adminFirebase');

router.get('/all', async (req, res) => {
    try{
        const airdrops = await firestore.collection('airdrops').orderBy('ends');
        const data = await airdrops.get();
        const airdropListsArray = [];
        if(data.empty){
            res.status(404).send("No airdrops");
        }else{
            data.forEach(doc => {
                const airdrop = new Airdrop(
                    doc.id,
                    doc.data().isFilled,
                    doc.data().iconUrl ,
                    doc.data().project ,
                    doc.data().reward ,
                    doc.data().website ,
                    doc.data().types ,
                    doc.data().searchTypes ,
                    doc.data().ends ,
                    doc.data().starts ,
                    doc.data().description ,
                    doc.data().status ,
                    doc.data().creatorId ,
                    doc.data().creatorName ,
                );
                airdropListsArray.push(airdrop)
            })
            res.send(airdropListsArray)
        }
    }catch (err) {
        res.status(400).send(err.message)
    }
})
router.post('/add-airdrop', async (req, res) => {
    const idToken = req.headers.authorization ;
    const airdrop = req.body;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];
    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const currentUserData = await firestore.collection('users').doc(user.uid);
        const currentUser = await currentUserData.get()
        if(!currentUser.data().isAdmin)
            res.status(401).send("UNAUTHORIZED REQUEST!");
        else{
            firestore.collection('airdrops').doc().set(airdrop).then((response) => {
                res.send(response)
            })
        }
    }catch (err) {
        res.status(400).send(err.message)
    }
})

router.get("/:id", async  (req, res)=> {
    const idToken = req.headers.authorization ;
    const id = req.params.id;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];
    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const airdrop = await firestore.collection("airdrops").doc(id)
        const data = await airdrop.get();
        if(!data.exists){
            res.status(404).send(`Airdrop not found`)
        }else{
            res.send(data.data())
        }
    }catch (err){
        res.status(400).send(err.message)
    }
});
router.get("/calendar/:startDay&:endaDay", async  (req, res)=> {
    try{
        const startDay = req.params.startDay;
        const endaDay = req.params.endaDay;
        const airdrops = await firestore.collection("airdrops");
        const filteredAirdrop =await airdrops.orderBy("ends").where("ends", ">=" ,parseInt(startDay)).where("ends", "<=" ,parseInt(endaDay))
        const data = await filteredAirdrop.get();
        const airdropListsArray = [];
        if(data.empty){
            res.status(404).send("No Airdrops");
        }else{
            data.forEach(doc => {
                const airdrop = new Airdrop(
                    doc.id,
                    doc.data().isFilled,
                    doc.data().iconUrl ,
                    doc.data().project ,
                    doc.data().reward ,
                    doc.data().website ,
                    doc.data().types ,
                    doc.data().searchTypes ,
                    doc.data().ends ,
                    doc.data().starts ,
                    doc.data().description ,
                    doc.data().status ,
                    doc.data().creatorId ,
                    doc.data().creatorName
                );
                airdropListsArray.push(airdrop)
            })
            res.send(airdropListsArray)
        }
    }catch (err){
        res.status(400).send(err.message)
    }
});
router.get("/type/:typeName", async  (req, res)=> {
    try{
        const typeName = req.params.typeName;
        const airdrops = await firestore.collection("airdrops").orderBy('ends');
        const filteredAirdrop =await airdrops.where("searchTypes", "array-contains" , typeName)
        const data = await filteredAirdrop.get();
        const airdropListsArray = [];
        if(data.empty){
            res.status(404).send("No Airdrops");
        }else{
            data.forEach(doc => {
                const airdrop = new Airdrop(
                    doc.id,
                    doc.data().isFilled,
                    doc.data().iconUrl ,
                    doc.data().project ,
                    doc.data().reward ,
                    doc.data().website ,
                    doc.data().types ,
                    doc.data().searchTypes ,
                    doc.data().ends ,
                    doc.data().starts ,
                    doc.data().description ,
                    doc.data().status ,
                    doc.data().creatorId ,
                    doc.data().creatorName ,
                );
                airdropListsArray.push(airdrop)
            })
            res.send(airdropListsArray)
        }
    }catch (err){
        res.status(400).send(err.message)
    }
})
router.put('/:id', async (req, res) => {
    const idToken = req.headers.authorization ;
    const id = req.params.id;
    const data = req.body;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];
    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const currentUserData = await firestore.collection('users').doc(user.uid);
        const currentUser = await currentUserData.get()
        if(!currentUser.data().isAdmin)
            res.status(401).send("UNAUTHORIZED REQUEST!");
        else{
            const airdrop = await firestore.collection("airdrops").doc(id);
            await airdrop.update(data);
            res.send("Airdrop record updated successfuly")
        }
    }catch (err){
        res.status(400).send(err.message)
    }
})
router.delete('/:id', async (req, res) => {
    const idToken = req.headers.authorization ;
    const id = req.params.id;

    if(!idToken)
        res.status(401).send("UNAUTHORIZED REQUEST!");

    const words = idToken.split(' ')
    if (words.length !== 2)
        res.status(401).send("Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.!");

    const token = words[1];
    try{
        const user = await  adminFirebase.auth().verifyIdToken(token);
        const currentUserData = await firestore.collection('users').doc(user.uid);
        const currentUser = await currentUserData.get()
        if(!currentUser.data().isAdmin)
            res.status(401).send("UNAUTHORIZED REQUEST!");
        else{
            await firestore.collection("airdrops").doc(id).delete();
            res.send("Airdrop record delete successfuly")
        }
    }catch (err){
        res.status(400).send(err.message)
    }
})
module.exports.route = router;