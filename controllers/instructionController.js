const express = require('express');
const router = express.Router();
const Instruction = require('../models/instruction');
const firebase = require('../db');
const firestore = firebase.firestore();

router.get('/all', async (req, res) => {
    try{
        const instructions = await firestore.collection('instructions');
        const data = await instructions.get();
        const instructionArray = [];
        if(data.empty){
            res.status(404).send("No instructions");
        }else{
            data.forEach(doc => {
                const instruction = new Instruction(
                    doc.id,
                    doc.data().name
                );
                instructionArray.push(instruction)
            })
            res.send(instructionArray)
        }
    }catch (err) {
        res.status(400).send(err.message)
    }
})


module.exports.route = router;