const admin = require("firebase-admin");


const serviceAccount = require("./serviceAccountKey.json");

const adminFirebase = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://fomo-61b78-default-rtdb.firebaseio.com",
    storageBucket: "gs://fomo-61b78.appspot.com"
});
module.exports = adminFirebase;